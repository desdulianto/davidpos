'''
Created on Mar 5, 2010

@author: david
'''

import wx

from controller import data, dialog, ProsesGiro, auth
from gui import FrameDataBank, FrameDataHargaJual, FrameDataBarang, \
                FramePembayaranHutang, FramePembayaranPiutang, \
                FrameTransaksiBank, FrameTransaksiPembelian, \
                FrameTransaksiPenjualan, FrameTransaksiReturPembelian, \
                FrameTransaksiReturPenjualan, FrameReminderGiro, \
                FrameUser, FrameGiro, FramePenyesuaianStock, DialogUbahPassword, \
                FrameLaporanStock, FrameLaporanPenjualan, FrameLaporanPembelian, \
                FrameLaporanReturPenjualan, FrameLaporanReturPembelian, \
                FrameLaporanPembayaranSupplier, FrameLaporanPembayaranCustomer, \
                FramePembayaranGiro
from gui import laporan
                
from gui import gui

restart = False
                
def _genfunc(parent, form, maximize=True):
    def OnClick(event): #IGNORE:W0613
        f = form(parent)
        
        if isinstance(f, wx.Dialog):
            f.ShowModal()
        else:
            f.Show()
        if maximize:
            f.Maximize()
    return OnClick

def aksesDitolak(parent):
    def func(parent):
        #msg = wx.MessageDialog(parent, "Akses ditolak", "Pelita Jaya", wx.OK)
        msg = wx.MessageDialog(parent, "Akses ditolak", "Aplikasi Bisnis", wx.OK)
        msg.ShowModal()
    return func(parent)
    

class PJFrame(gui.FrameUtama):
    def __init__(self, parent):
        gui.FrameUtama.__init__(self, parent)
        self.Title = self.Title + " - " + auth.user.user
        menus = [(self.menuItemSupplier, data.Supplier),
                 (self.menuItemCustomer, data.Customer),
                 (self.menuItemBarang, FrameDataBarang.FrameDataBarang),
                 (self.menuItemHargaJual, FrameDataHargaJual.FrameDataHargaJual),
                 (self.menuItemSatuan, data.Satuan),
                 (self.menuItemKategori, data.Kategori),
#                 (self.menuItemBank, FrameDataBank.FrameDataBank),
                 (self.menuItemPenjualan, FrameTransaksiPenjualan.FrameTransaksiPenjualan),
                 (self.menuItemReturPenjualan, FrameTransaksiReturPenjualan.FrameTransaksiReturPenjualan),
                 (self.menuItemPembelian, FrameTransaksiPembelian.FrameTransaksiPembelian),
                 (self.menuItemReturPembelian, FrameTransaksiReturPembelian.FrameTransaksiReturPembelian),
#                 (self.menuItemPembayaranSupplier, FramePembayaranHutang.FramePembayaranHutangTunai),
#                 (self.menuItemPembayaranCustomer, FramePembayaranPiutang.FramePembayaranPiutangTunai),
#                 (self.menuItemTransaksiBank, FrameTransaksiBank.FrameTransaksiBank),
#                 (self.menuItemReminderGiro, FrameReminderGiro.FrameReminderGiro, False),
                 (self.menuItemAdministrasiPengguna, FrameUser.FrameUser),
#                 (self.menuItemGiro, FrameGiro.FrameGiro),
                 (self.menuItemPenyesuaianStock, FramePenyesuaianStock.FramePenyesuaianStock2),
                 (self.menuItemUbahPassword, DialogUbahPassword.DialogUbahPassword, False),
                 (self.menuItemLaporanTransaksiStock, laporan.FrameLaporanTransaksiStock),
#                 (self.menuItemLaporanStock, FrameLaporanStock.FrameLaporanStock),
                 (self.menuItemLaporanPenjualan, FrameLaporanPenjualan.FrameLaporanPenjualan),
                 (self.menuItemLaporanPembelian, FrameLaporanPembelian.FrameLaporanPembelian),
                 (self.menuItemLaporanReturPenjualan, FrameLaporanReturPenjualan.FrameLaporanReturPenjualan),
                 (self.menuItemLaporanReturPembelian, FrameLaporanReturPembelian.FrameLaporanReturPembelian),
#                 (self.menuItemLaporanPembayaranSupplier, FrameLaporanPembayaranSupplier.FrameLaporanPembayaranSupplier),
#                 (self.menuItemLaporanPembayaranCustomer, FrameLaporanPembayaranCustomer.FrameLaporanPembayaranCustomer),
#                 (self.menuItemPembayaranGiroSupplier, FramePembayaranGiro.FramePembayaranGiroSupplier, False),
#                 (self.menuItemPembayaranGiroCustomer, FramePembayaranGiro.FramePembayaranGiroCustomer, False)]
                ]
        for menu in menus:
            if not auth.user.has_permission("LIHAT", menu[1].__nama__):
                self.m_menubar1.Enable(menu[0].GetId(), False)
                continue
            
            # for maximize
            if len(menu) == 3:
                f = _genfunc(self, menu[1], menu[2])
            else:
                f = _genfunc(self, menu[1])
            self.Bind(wx.EVT_MENU, f, menu[0])
                        
            
        # user logout
        self.Bind(wx.EVT_MENU, self.logout, self.menuItemLogout)
        
        # save form
        self.Bind(wx.EVT_MENU, self.simpan, self.saveMenu)
        
        # close frame
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        
                        
        # shortcut keys
        closeWindowMenu = self.GetMenuBar().FindMenuItem("Window", "Close")
        previousWindowMenu = self.GetMenuBar().FindMenuItem('Window', 'Previous')
        nextWindowMenu = self.GetMenuBar().FindMenuItem('Window', 'Next')
        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL, ord('W'), closeWindowMenu),
                                         (wx.ACCEL_CTRL, ord('S'), self.saveMenu.GetId()),
                                         (wx.ACCEL_CTRL, wx.WXK_PAGEUP, previousWindowMenu),
                                         (wx.ACCEL_CTRL, wx.WXK_PAGEDOWN, nextWindowMenu)])
        
        self.SetAcceleratorTable(accel_tbl)
        
            
        # proses pembayaran giro
        #self.Bind(wx.EVT_MENU, ProsesGiro.ProsesGiro, self.menuItemProsesGiro)
        #prosesGiro = ProsesGiro.ProsesGiro()
        #del prosesGiro
            
        #reminderGiro = FrameReminderGiro.FrameReminderGiro(self)
        #reminderGiro.Show()
    
    def logout(self, event):
        #msg = wx.MessageDialog(self, "Yakin Logout?", "Pelita Jaya",
        #                       wx.YES_NO|wx.ICON_INFORMATION)
        msg = wx.MessageDialog(self, "Yakin Logout?", "Aplikasi Bisnis",
                               wx.YES_NO|wx.ICON_INFORMATION)
        if msg.ShowModal() == wx.ID_NO:
            return    
        global restart
        restart = True
        self.Destroy()
        
    def OnClose(self, event):
        msg = wx.MessageDialog(self, "Yakin Tutup?", "Aplikasi Bisnis",
                               wx.YES_NO|wx.ICON_QUESTION)
        hasil = msg.ShowModal()
        if hasil == wx.ID_YES:
            self.Destroy()
        else:
            event.Veto()
            
    def simpan(self, event):
        client = self.GetActiveChild()
        if hasattr(client, 'simpan'):
            client.simpan()
        else:
            pass

        
def showLoginDialog():
    login = dialog.Login(None)
    
    while True:
        ret = login.ShowModal()
        if ret == wx.ID_OK:
            if not login.getAuth():
                msg = wx.MessageDialog(None, "User/Password Salah", "Login", wx.OK)
                msg.ShowModal()
                login.textUser.SetFocus()
            else:
                return login
        else:
            return None
    return login

app = wx.PySimpleApp(0)
wx.InitAllImageHandlers()

#login = dialog.Login(None)
#
#while True:
#    ret = login.ShowModal()
#    if ret == wx.ID_OK:
#        if not login.getAuth():
#            msg = wx.MessageDialog(None, "User/Password Salah", "Login", wx.OK)
#            msg.ShowModal()
#        else:
#            break
#    else:
#        break
#    
#if login.getAuth():
#    frm = PJFrame(None)
#    frm.Maximize(True)
#    frm.Show()
#    app.SetTopWindow(frm)
while (True):
    login = showLoginDialog()
    if login is None:
        break
    if login.getAuth():
        restart = False
        frm = PJFrame(None)
        frm.Maximize(True)
        frm.Show()
        app.SetTopWindow(frm)
        
    login.Destroy()

    app.MainLoop()
    if not restart:
        break