#!/bin/bash
mkdir backup 2>/dev/null
tanggal=`date +%d%m%y%s`
bzip2 -9 -z -k db.sqlite
mv db.sqlite.bz2 backup/db-$tanggal.sqlite.bz2
