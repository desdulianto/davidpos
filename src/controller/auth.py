'''
Created on 09 Mei 2010

@author: David
'''

from db import pelitajaya as pj
from hashlib import sha1
import sqlalchemy

user = None

def auth(u, password):
    '''
    meng-autentikasi user
    apabila berhasil, global variable user diisikan dengan object user
    jika tidak berhasil, global variable user == None
    @param u: nama user (string)
    @param password: password untuk login
    @return True jika berhasil, False jika gagal
    '''
    global user
    try:
        ou = pj.User.query.filter_by(user=u).one()
        if ou.password == sha1(password).hexdigest() and ou.active:
            user = ou
            return True
        else:
            return False
    except sqlalchemy.orm.exc.NoResultFound:
        return False
    except sqlalchemy.orm.exc.MultipleResultsFound:
        return False
    
def checkPassword(u, password):
    '''
    validasi password
    @param u: object user
    @param password: password
    @return True/False
    '''
    return u.password == sha1(password).hexdigest()

def updatePassword(u, passwordBaru):
    '''
    update password user
    @param u: object user
    @param passwordBaru: password
    @return True/False
    '''
    try:
        u.password = sha1(passwordBaru).hexdigest()
        pj.session.commit()
    except Exception, e:
        return False
    return True
    
def newUser(user, password):
    '''
    menambahkan user baru
    @param user: nama user (string)
    @param password: password user (string)
    @return True jika berhasil, False jika gagal
    '''
    
    try:
        userBaru = pj.User()
        userBaru.user = user
        userBaru.password = sha1(password).hexdigest()
        userBaru.active = True
        pj.session.commit()
    except Exception, e:
        return False
    
    return True
    
def activateUser(user, active=True):
    '''
    mengaktifkan/meng-nonaktifkan user
    user yang nonaktif tidak dapat login ke system
    @param user: user yang akan diaktifkan/nonaktifkan (object user/string)
    @param active: flag aktif (boolean), jika tidak diisi maka active = True
    @return True jiga berhasil, False jika gagal
    '''
    if type(user) == type(''):
        try:
            user = pj.User.query.filter(pj.User.user == user).one()
        except sqlalchemy.orm.exc.NoResultFound, e:
            return False
        except sqlalchemy.orm.exc.MultipleResultsFound, e:
            return False
        
    user.active = active
    pj.session.commit()

def logout():
    '''
    meng-logout user dari system
    global variable user diset menjadi none
    '''
    user = None