def integerFormatString(value, digitGroup='.'):
	if type(value) != type(0) and type(value) != type(0L):
		raise ValueError, "%s is a %s" % (value, type(value))

	# reverse string
	s = str(value)[::-1]
	r = []
	count = 0
	for c in s:
		if count > 0 and count % 3 == 0:
			r.append(digitGroup)
		r.append(c)
		count = count + 1
	return ''.join(r)[::-1]

if __name__ == '__init__':
	print integerFormatString(1000000)
	print integerFormatString(2000000)
	print integerFormatString(10000)
	print integerFormatString(120000)
	print integerFormatString(0)
	print integerFormatString(90034352356326323532532525325562)
	print integerFormatString(3432532532035235256326326313423525252)
	print integerFormatString(3525236235632)
	print integerFormatString(3.4)
