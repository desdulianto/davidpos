'''
Created on 28 Mar 2010

@author: David
'''

from db import pelitajaya as pj

class ItemBarang(object):
    def __init__(self):
        self.id = ""
        self.nama = ""
        self.hargaModal = 0
        self.hargaJual = 0
        self.satuan = None
        self.isi = ""

class DataBarang(object):
    def __init__(self, barang=None):
        self.barang = barang
        
    def all(self):
        #=======================================================================
        # seluruh daftar barang
        #=======================================================================
        self.daftarBarang = []
        
        for b in pj.Barang.query.order_by(pj.Barang.id).all():
            item = ItemBarang()
            item.id = b.id
            item.nama = b.nama
            item.hargaModal = b.modalBarang
            item.hargaJual = b.hargaJual
            item.satuan = b.satuan
            item.isi = b.isi
            self.daftarBarang.append(item)