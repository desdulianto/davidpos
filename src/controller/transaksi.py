'''
Created on 07 Mar 2010

@author: David
'''

from gui.gui import *
from gui import gui
from db import pelitajaya as pj
from sqlalchemy import and_, or_
import sqlalchemy
import wx
from datetime import date

from ObjectListView import ColumnDefn, ObjectListView as olv
import ObjectListView
from util import integerFormatString, makeNumCtrlEditor, MyComboBox, populateComboBox

def makeComboBoxEditor(list, row, item):
    barangs = pj.Barang.query.all()
    choices = []
    editor = MyComboBox(list, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, 
                        wx.DefaultSize, choices, 0)    
    populateComboBox(editor, barangs)
#    editor.Append("<<Tambah Barang Baru>>", -1)
    return editor

class ItemDetail(object):
    def __init__(self):
        self.barang = None
        self.harga = 0
        self.qty = 0
        self.disc = 0
        
    def subTotal(self):
        return (self.harga - self.disc) * self.qty
    
    def namaBarang(self):
        if self.barang is not None:
            return self.barang.display()
        else:
            return ''
    
class Pembelian(gui.FrameTransaksiPembelian):
    def __init__(self, parent):
        gui.FrameTransaksiPembelian.__init__(self, parent)
        
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        
        # combo supplier
        self.comboSupplier.SetFocus()
        suppliers = pj.Supplier.query.all()
        self.comboSupplier.Bind(wx.EVT_COMBOBOX, self.OnComboBox)
        self.comboSupplier.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterComboBox)        
        populateComboBox(self.comboSupplier, suppliers)

                   
        # database object
        self.header = None
        self.detail = []
        
        # combo barang
        self.comboBarang.Bind(wx.EVT_COMBOBOX, self.OnComboBox)
        barangs = pj.Barang.query.all()
        
#        for barang in barangs:
#            self.frm.comboBarang.Append(barang.display(), barang)
        populateComboBox(self.comboBarang, barangs)
#        self.comboBarang.Append("<<Tambah Barang Baru>>", -1)
        self.comboBarang.Bind(wx.EVT_COMBOBOX, self.OnBarangComboBox)
            
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahButton)
        
        # textHarga & textQty
        #self.frm.Bind(wx.EVT_TEXT_ENTER, self.HitungSubTotal, self.frm.textHarga)
        #self.frm.Bind(wx.EVT_TEXT_ENTER, self.HitungSubTotal, self.frm.textQty)
        self.numHarga.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        self.numQty.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)   
#        self.frm.numHarga.Bind(wx.EVT_LEFT_DOWN, self.SelectNumCtrl)
#        self.frm.numQty.Bind(wx.EVT_LEFT_DOWN, self.SelectNumCtrl)
        
        # objListDetail
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        self.objListDetail.SetColumns([
            ColumnDefn("Barang", "left", 400, "namaBarang", isSpaceFilling=True,
                       cellEditorCreator=makeComboBoxEditor),
            ColumnDefn("Harga Satuan", "right", 150, "harga", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateHarga),
            ColumnDefn("Qty", "right", 150, "qty", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateQty),
            ColumnDefn("Sub Total", "right", 200, "subTotal", 
                       stringConverter=integerFormatString, isEditable=False)
        ])
        self.objListDetail.SetObjects(self.detail)
        self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        self.objListDetail.rowFormatter = self.detailRowFormatter
        self.objListDetail.Bind(wx.EVT_LIST_INSERT_ITEM, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_LIST_DELETE_ITEM, lambda(event): self.hitungTotal())
        #self.frm.objListDetail.Bind(ObjectListView.EVT_CELL_EDIT_FINISHING, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
        
        self.buttonSimpan.Bind(wx.EVT_BUTTON, self.OnSimpanButton)
        
        # listDetail
#        ld = self.frm.listDetail
#        cols = ['Barang', 'Harga Satuan', 'Qty', 'Sub Total']
#        for i in range(len(cols)):
#            ld.InsertColumn(i, cols[i])
        
    def HitungSubTotal(self, event):
        qty = self.numQty.GetValue()
        harga = self.numHarga.GetValue()
        
        self.numSubTotal.SetValue(qty * harga)
        event.Skip()
    
#    def SelectNumCtrl(self, event):
#        obj = event.GetEventObject()
#        obj.SelectAll()
#        print 'selected'
#        event.Skip()        
        
    def updateBarang(self, model, value):
        model.barang = value        

    def updateHarga(self, model, value):
        model.harga = value
        self.hitungTotal()
        
    def updateQty(self, model, value):
        model.qty = value
        self.hitungTotal()
        
    def detailRowFormatter(self, listItem, model):
        if model.harga > 0:
            listItem.SetTextColour(wx.RED)
                    
    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == 127:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self.panel, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                self.objListDetail.RemoveObject(obj)
                self.hitungTotal()    
            hapus.Destroy()
        else:
            event.Skip()
            
    def OnBarangComboBox(self, event):
        n = self.comboBarang.GetSelection()
        if n == wx.NOT_FOUND:
            return
        barang = self.comboBarang.GetClientData(n)        
        
        harga = barang.hargaModal
        
        if harga is None:
            harga = 0
        
        self.numHarga.SetValue(harga)                  
        
    def OnComboBox(self, event):
        i = self.comboSupplier.GetSelection()    
        d = self.comboSupplier.GetClientData(i)
        if d == -1:
            import data
            data.Supplier(self).Show()
        pass
#        obj = event.GetEventObject()
#        if obj == self.frm.comboSupplier:
#            n = obj.GetSelection()
#            if not n == wx.NOT_FOUND:
#                print obj.GetClientData(n)
#        elif obj == self.frm.comboBarang:
#            print 'on barang'
            
    def OnTextEnterComboBox(self, event):
#        print 'enter'
#        obj = event.GetEventObject()
#        n = obj.GetSelection()
#        if n == wx.NOT_FOUND:
#            return
#        if obj == self.frm.comboSupplier:
#            print 'tambah data supplier baru'
#        elif obj == self.frm.comboBarang:        
#            print 'tambah data barang baru'
        pass
    
    def OnTambahButton(self, event):
        if (self.comboBarang.GetSelection() == wx.NOT_FOUND or 
            self.numHarga.GetValue() <= 0 or self.numQty.GetValue() <= 0):
            msg = wx.MessageDialog(self, "Pesanan belum diisi dengan lengkap", 
                                   "Transaki Pembelian", wx.OK)
            msg.ShowModal()
            return
               
        detail = pj.BeliDetail()
        detail.header = self.header
        detail.barang = self.comboBarang.GetClientData(self.comboBarang.GetSelection())
        detail.harga = int(self.numHarga.GetValue())
        detail.qty = int(self.numQty.GetValue())
        #self.detail.append(detail)
        objs = self.objListDetail.GetObjects()
        
        # jika barang sudah ada dan harga sama, update qty barang
        exist = False
        for obj in objs:
            if obj.barang == detail.barang and obj.harga == detail.harga:
                obj.qty = obj.qty + detail.qty
                self.objListDetail.RefreshObject(obj)
                self.hitungTotal()
                exist = True
                break
        
        if not exist:
            self.objListDetail.AddObject(detail)
        
#        self.hitungTotal()
        
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numQty.Clear()
        self.numHarga.Clear()
        self.numSubTotal.Clear()
        
        self.comboBarang.SetFocus()
        
    
    def hitungTotal(self):
        objs = self.objListDetail.GetObjects()
        total = 0
        for obj in objs:
            total = total + obj.subTotal()
        self.numTotal.SetValue(total)   
    
    def OnSimpanButton(self, event):
        if self.header == None:
            self.header = pj.BeliHeader()
        self.header.supplier = self.comboSupplier.GetClientData(self.comboSupplier.GetSelection())
        tanggal = self.pickerTanggal.GetValue()        
        self.header.tanggal = date(tanggal.Year, tanggal.Month+1, tanggal.Day)        
        jatuhTempo = self.pickerJatuhTempo.GetValue()
        self.header.jatuhTempo = date(jatuhTempo.Year, jatuhTempo.Month+1, jatuhTempo.Day)        
        self.header.noFaktur = self.textNoFaktur.GetValue().strip().upper()
        
        self.detail = self.objListDetail.GetObjects()
        if self.header != None and len(self.detail) > 0:
            stock = []
            for item in self.detail:
                item.header = self.header
                stock.append(pj.Stock())
                stock[-1].barang = item.barang
                stock[-1].tanggal = self.header.tanggal
                stock[-1].hargaModal = item.harga
                stock[-1].qty = item.qty
                
                item.barang.hargaModal = item.harga                            
            try:
                pj.session.commit()
                self.clearAll()
            except sqlalchemy.exc.IntegrityError:
                pj.session.rollback()
                msg = wx.MessageDialog(self, "No Faktur Sudah Terdaftar", "Transaksi Pembelian", wx.OK)
                msg.ShowModal()
        
        self.comboSupplier.SetFocus()
    
    def clearAll(self):
        self.header = None
        self.detail = []
        self.objListDetail.DeleteAllItems()
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        self.numTotal.Clear()
        self.comboSupplier.SetSelection(wx.NOT_FOUND)
        self.comboSupplier.SetValue("")
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        self.pickerJatuhTempo.SetValue(wx.DateTime.Today())
        self.textNoFaktur.SetValue("")    
        
class Penjualan(gui.FrameTransaksiPenjualan):
    def __init__(self, parent):
        gui.FrameTransaksiPenjualan.__init__(self, parent)
        
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        
        # combo customer
        self.comboCustomer.SetFocus()
        customers = pj.Customer.query.all()
        populateComboBox(self.comboCustomer, customers)
#        self.comboCustomer.Append("<<Tambah Customer Baru>>", -1)
        self.comboCustomer.Bind(wx.EVT_COMBOBOX, self.OnCustomerComboBox)
        
        # database object
        self.header = None
        self.detail = []
        
        # combo sales
        sales = pj.Sales.query.all()
        populateComboBox(self.comboSales, sales)
#        self.comboSales.Append("<<Tambah Sales Baru>>", -1)
        
        # combo barang
        barangs = pj.Barang.query.all()
        populateComboBox(self.comboBarang, barangs)
#        self.comboBarang.Append("<<Tambah Barang Baru>>", -1)
        self.comboBarang.Bind(wx.EVT_COMBOBOX, self.OnBarangComboBox)

        self.numHarga.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        self.numQty.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        self.numDisc.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        
        # objListDetail
        self.objListDetail.SetColumns([
#            ColumnDefn("Barang", "left", 400, "namaBarang", 
#                       stringConverter=self.barangFormatString,
#                       isSpaceFilling=True, cellEditorCreator=makeComboBoxEditor,
#                       valueSetter=self.updateBarang),
            ColumnDefn("Barang", "left", 400, "namaBarang", 
                       stringConverter=self.barangFormatString,
                       isSpaceFilling=True, isEditable=False),                       
            ColumnDefn("Harga Satuan", "right", 150, "harga", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateHarga),
            ColumnDefn("Disc", "right", 150, "disc",
                       stringConverter=integerFormatString,
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateDisc),
            ColumnDefn("Qty", "right", 150, "qty", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateQty),
            ColumnDefn("Sub Total", "right", 200, "subTotal", 
                       stringConverter=integerFormatString, isEditable=False)
        ])
        self.objListDetail.SetObjects(self.detail)
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        #self.objListDetail.rowFormatter = self.detailRowFormatter
        self.objListDetail.Bind(wx.EVT_LIST_INSERT_ITEM, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_LIST_DELETE_ITEM, lambda(event): self.hitungTotal())
        #self.frm.objListDetail.Bind(ObjectListView.EVT_CELL_EDIT_FINISHING, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_KEY_DOWN, self.OnListDetailKeyDown)
        self.numHarga.Bind(wx.EVT_KEY_DOWN, self.OnEditHargaEsc)
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        self.buttonSimpan.Bind(wx.EVT_BUTTON, self.OnSimpanClick)
        
    def barangFormatString(self, value):
        return value.display()
    
    def updateBarang(self, model, value):
        model['namaBarang'] = value
        
    def updateHarga(self, model, value):
        model['harga'] = value
        model['subTotal'] = (model['harga']-model['disc']) * model['qty']
        self.hitungTotal()
        
    def updateDisc(self, model, value):
        model['disc'] = value
        model['subTotal'] = (model['harga']-model['disc']) * model['qty']
        self.hitungTotal()
        
    def updateQty(self, model, value):
        model['qty'] = value
        model['subTotal'] = (model['harga']-model['disc']) * model['qty']
        #FIXME: tambahkan pemeriksaan untuk qty > stock    
        self.hitungTotal()
        
    def hitungTotal(self):
        objs = self.objListDetail.GetObjects()
        total = 0
        for obj in objs:
            #total = total + obj.subTotal()
            total = total + obj['subTotal']
        self.numTotal.SetValue(total)
        
    def HitungSubTotal(self, event):
        qty = self.numQty.GetValue()
        disc = self.numDisc.GetValue()
        harga = self.numHarga.GetValue()            
        
        try:
            self.numSubTotal.SetValue(qty * (harga-disc))
        except ValueError:
            msg = wx.MessageDialog(self, "Harga/Qty/Disc salah", "Transaksi Penjualan", wx.OK | wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numHarga.SetFocus()
        event.Skip()

    def OnListDetailKeyDown(self, event):
        if event.GetKeyCode() == 127:
            i = self.objListDetail.GetFocusedRow()
            if i < 0:
                return
            hapus = wx.MessageDialog(self.panel, 'Yakin hapus?', 'Hapus', wx.YES_NO|wx.ICON_QUESTION)
            ret = hapus.ShowModal()
            if ret == wx.ID_YES:
                obj = self.objListDetail.GetObjectAt(i)
                self.objListDetail.RemoveObject(obj)
                self.hitungTotal()    
            hapus.Destroy()
        else:
            event.Skip()
            
    def getHargaDefault(self):
        n = self.comboBarang.GetSelection()
        if n != wx.NOT_FOUND:
            barang = self.comboBarang.GetClientData(n)
        else:
            barang = None
            return 0
            
        n = self.comboCustomer.GetSelection()
        if n != wx.NOT_FOUND:
            customer = self.comboCustomer.GetClientData(n)
        else:
            customer = None
            return 0
            
        harga = pj.HargaJual.query.filter(and_(pj.HargaJual.barang==barang,
                                                  pj.HargaJual.customer==customer))
        if harga.first() is None:
            harga = barang.hargaJual
        else:
            harga = harga[0].harga
        return harga
            
    def OnEditHargaEsc(self, event):
        if event.GetKeyCode() == 27: # ESC
            self.numHarga.SetValue(self.getHargaDefault())
        event.Skip()
            
    def OnBarangComboBox(self, event):       
        self.numHarga.SetValue(self.getHargaDefault())
        event.Skip()
        
    def OnCustomerComboBox(self, event):
        self.numHarga.SetValue(self.getHargaDefault())
        event.Skip()
        
    def OnTambahClick(self, event):
        n = self.comboBarang.GetSelection()
        if n != wx.NOT_FOUND:
            nb = self.comboBarang.GetClientData(n)
        else:
            msg = wx.MessageDialog(self, "Barang belum diisi", 
                                   "Transaksi Penjualan", 
                                   wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboBarang.SetFocus()
            return
        if self.numHarga.GetValue() <= 0:
            msg = wx.MessageDialog(self, "Harga salah",
                                   "Transaksi Penjualan", 
                                   wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numHarga.SetFocus()
            return
        if self.numQty.GetValue() <= 0:
            msg = wx.MessageDialog(self, "Qty salah",
                                   "Transaksi Penjualan",
                                   wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numQty.SetFocus()
            return
        if self.numQty.GetValue() > nb.jumlahStock():
            msg = wx.MessageDialog(self, "Stock tidak mencukupi: " + str(nb.jumlahStock()),
                                   "Transaksi Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numQty.SetFocus()
            return
        if self.numDisc.GetValue() >= self.numHarga.GetValue():
            msg = wx.MessageDialog(self, "Disc tidakb oleh lebih besar dari harga",
                                   "Transaksi Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numDisc.SetFocus()
            return
        
        barang = {}        
        barang['namaBarang'] = nb
        barang['harga'] = self.numHarga.GetValue()
        barang['disc'] = self.numDisc.GetValue()
        barang['qty'] = self.numQty.GetValue()
        barang['subTotal'] = barang['harga'] * barang['qty']
        
        exist = False
        for brg in self.objListDetail.GetObjects():
            if (barang['namaBarang'] == brg['namaBarang'] and 
                barang['harga'] == brg['harga']):
                if brg['qty'] + barang['qty'] > nb.jumlahStock():
                    msg = wx.MessageDialog(self, "Stock tidak mencukupi: " + str(nb.jumlahStock()),
                                           "Transaksi Penjualan", wx.OK|wx.ICON_INFORMATION)
                    msg.ShowModal()
                    self.numQty.SetFocus()
                    return
                brg['qty'] = brg['qty'] + barang['qty']
                brg['subTotal'] = brg['harga'] * brg['qty']
                exist = True
                self.objListDetail.RefreshObject(brg)
                self.hitungTotal()
                break
        if not exist:                
            self.objListDetail.AddObject(barang)
        
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numDisc.Clear()
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        
        self.comboBarang.SetFocus()

    def OnSimpanClick(self, event):
        if self.header == None:
            self.header = pj.JualHeader()
        self.header.customer = self.comboCustomer.GetClientData( 
                                            self.comboCustomer.GetSelection() )
        self.header.sales = self.comboSales.GetClientData(
                                            self.comboSales.GetSelection() )
        self.header.noFaktur = self.textNoFaktur.GetValue().strip().upper()
        tanggal = self.pickerTanggal.GetValue()
        self.header.tanggal = date(tanggal.Year, tanggal.Month+1, tanggal.Day)
        
        # stock dan jual detail
        items = self.objListDetail.GetObjects()
        
        for item in items:
            stock = pj.Stock.query.filter_by(barang=item['namaBarang']).order_by(pj.Stock.tanggal)
            jumlah = item['qty']
            for i in stock.all():
                if i.qty > jumlah:
                    n = jumlah
                else:
                    n = i.qty
                jumlah = jumlah - n
                #print i.tanggal, i.hargaModal, i.qty, i.qty-n, n
                detail = pj.JualDetail()
                detail.header = self.header
                detail.barang = item['namaBarang']
                detail.hargaJual = item['harga']
                detail.disc = item['disc']
                detail.hargaModal = i.hargaModal
                detail.qty = n
                i.qty = i.qty - n
                if jumlah <= 0:
                    break
                
        pj.session.commit()
        self.clearAll()
        
    def clearAll(self):
        self.comboCustomer.SetSelection(wx.NOT_FOUND)
        self.comboCustomer.SetValue("")
        self.comboSales.SetSelection(wx.NOT_FOUND)
        self.comboSales.SetValue("")
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        self.textNoFaktur.Clear()
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numDisc.Clear()
        self.numHarga.Clear()
        self.numQty.Clear()        
        self.numSubTotal.Clear()
        self.objListDetail.DeleteAllItems()
        self.numTotal.Clear()
        
        self.comboCustomer.SetFocus()
        

        
class ReturPembelian(gui.FrameTransaksiReturPembelian):
    def __init__(self, parent):
        gui.FrameTransaksiReturPembelian.__init__(self, parent)
        
        # tanggal hari ini
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        
        # combo supplier
        self.comboSupplier.SetFocus()
        suppliers = pj.Supplier.query.all()
#        self.comboSupplier.Bind(wx.EVT_COMBOBOX, self.OnSupplierComboBox)
#        self.comboSupplier.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterComboBox)        
        populateComboBox(self.comboSupplier, suppliers)
#        self.comboSupplier.Append("<<Tambah Supplier Baru>>", -1)
                
        # database object
        self.header = None
        self.detail = []
        
        # combo barang
        barangs = pj.Barang.query.all()
        populateComboBox(self.comboBarang, barangs)
#        self.comboBarang.Append("<<Tambah Barang Baru>>", -1)
        self.comboBarang.Bind(wx.EVT_COMBOBOX, self.OnBarangComboBox)
        
        self.numHarga.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        self.numQty.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        
        # list detail
        self.objListDetail.SetColumns([
            ColumnDefn("Barang", "left", 400, "namaBarang",
                       cellEditorCreator=makeComboBoxEditor,                        
                       isSpaceFilling=True),                       
            ColumnDefn("Harga Satuan", "right", 150, "harga", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateHarga),
            ColumnDefn("Qty", "right", 150, "qty", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateQty),
            ColumnDefn("Sub Total", "right", 200, "subTotal", 
                       stringConverter=integerFormatString, isEditable=False)
        ])
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        self.objListDetail.Bind(wx.EVT_LIST_INSERT_ITEM, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_LIST_DELETE_ITEM, lambda(event): self.hitungTotal())
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        self.buttonSimpan.Bind(wx.EVT_BUTTON, self.OnSimpanClick)
    
    def updateHarga(self, model, value):
        model.harga = value
    
    def updateQty(self, model, value):
        model.qty = value
        
    def HitungSubTotal(self, event):
        qty = self.numQty.GetValue()
        harga = self.numHarga.GetValue()
        self.numSubTotal.SetValue(qty * harga)
    
    def HargaPembelian(self, barang):
        brg = pj.Barang.query.filter_by(id=barang.id).first()
        if brg is None:
        	harga = 0
        else:
            harga = brg.hargaModal
            if harga is None:
                harga = 0
        return harga
    
    def OnBarangComboBox(self, event):
        n = self.comboBarang.GetSelection()
        if n == wx.NOT_FOUND:
            return
        else:
            barang = self.comboBarang.GetClientData(n)
            self.numHarga.SetValue(self.HargaPembelian(barang))
            
    def OnTambahClick(self, event):        
        n = self.comboBarang.GetSelection()
        if n == wx.NOT_FOUND:
            msg = wx.MessageDialog(self, "Barang belum dipilih", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboBarang.SetFocus()
            return
        else:
            barang = self.comboBarang.GetClientData(n)
            
        harga = self.numHarga.GetValue()
        qty = self.numQty.GetValue()
        
        if harga <= 0:
            msg = wx.MessageDialog(self, "Harga salah", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numHarga.SetFocus()
            return
            
        try:
            stock = pj.Barang.query.filter_by(id=barang.id).one()
        except:
            stock = 0
        
        if stock != 0:
            stock = stock.jumlahStock()
        if qty > stock:
            msg = wx.MessageDialog(self, "Qty melebihi stock: " + str(stock), "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numQty.SetFocus()
            return
        elif qty <= 0:
            msg = wx.MessageDialog(self, "Qty salah", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numQty.SetFocus()
            return
        
        #self.objListDetail.AddObject()
        
        detail = ItemDetail()
        detail.barang = barang
        detail.harga = harga
        detail.qty = qty
        exist = False
        for item  in self.objListDetail.GetObjects():
            if detail.barang == item.barang and detail.harga == item.harga:
                if item.qty + detail.qty > stock:
                    msg = wx.MessageDialog(self, "Qty melebihi stock: " + str(stock), "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
                    msg.ShowModal()
                    self.numQty.SetFocus()
                    return
                item.qty = item.qty + detail.qty
                exist = True
                self.objListDetail.RefreshObject(item)
                self.hitungTotal()                
                break
        if not exist:
            self.objListDetail.AddObject(detail)
        
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        self.comboBarang.SetFocus()
        
    def OnSimpanClick(self, event):
        n = self.comboSupplier.GetSelection()
        if n == wx.NOT_FOUND:
            msg = wx.MessageDialog(self, "Supplier belum diisi", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboSupplier.SetFocus()
            return
        else:
            supplier = self.comboSupplier.GetClientData(n)
        
        noRetur = self.textNoRetur.GetValue().strip().upper()
        if noRetur == "":
            msg = wx.MessageDialog(self, "No Retur belum diisi", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.textNoRetur.SetFocus()
            return
        
        if self.objListDetail.GetItemCount() <= 0:
            msg = wx.MessageDialog(self, "Barang retur belum diisi", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboBarang.SetFocus()
            return
        
        tanggal = self.pickerTanggal.GetValue()
        tanggal = date(tanggal.Year, tanggal.Month+1, tanggal.Day)
        
        returPembelian = pj.ReturPembelianHeader()
        returPembelian.noFaktur = noRetur
        returPembelian.supplier = supplier
        returPembelian.tanggal = tanggal
        
        details = []
        
        for item in self.objListDetail.GetObjects():
            detail = pj.ReturPembelianDetail()
            detail.barang = item.barang
            detail.harga = item.harga
            detail.qty = item.qty
            
            stock = pj.Stock.query.filter_by(barang=item.barang).order_by(pj.Stock.tanggal).all()
            jumlah = item.qty
            for s in stock:
                if s.qty > jumlah:
                    n = jumlah
                else:
                    n = s.qty
                jumlah = jumlah - n
                s.qty = s.qty - n
                if jumlah <= 0:
                    break
            details.append(detail)
            
        returPembelian.details = details
        pj.session.commit()
        
        self.clearAllInput()
        self.comboSupplier.SetFocus()
        
    def clearAllInput(self):
    	self.comboSupplier.SetSelection(wx.NOT_FOUND)
        self.comboSupplier.SetValue("")
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        self.textNoRetur.Clear()
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        self.objListDetail.ClearAll()
        self.numTotal.Clear()
        
    def hitungTotal(self):
        total = 0
        for item in self.objListDetail.GetObjects():
            total = total + item.subTotal()
            
        self.numTotal.SetValue(total)

class ReturPenjualan(gui.FrameTransaksiReturPenjualan):
    def __init__(self, parent):
        gui.FrameTransaksiReturPenjualan.__init__(self, parent)

        # tanggal hari ini
        self.pickerTanggal.SetValue(wx.DateTime.Today())

        # combo customer
        self.comboCustomer.SetFocus()
        customers = pj.Customer.query.all()
#        self.comboSupplier.Bind(wx.EVT_COMBOBOX, self.OnSupplierComboBox)
#        self.comboSupplier.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnterComboBox)        
        populateComboBox(self.comboCustomer, customers)
#        self.comboCustomer.Append("<<Tambah Customer Baru>>", -1)
        
        # combo barang
        barangs = pj.Barang.query.all()
        populateComboBox(self.comboBarang, barangs)
#        self.comboBarang.Append("<<Tambah Barang Baru>>", -1)
        self.comboBarang.Bind(wx.EVT_COMBOBOX, self.OnBarangComboBox)
        
        self.numHarga.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)
        self.numQty.Bind(wx.lib.masked.EVT_NUM, self.HitungSubTotal)

        # list detail
        self.objListDetail.SetColumns([
            ColumnDefn("Barang", "left", 400, "namaBarang",
                       cellEditorCreator=makeComboBoxEditor,                        
                       isSpaceFilling=True),                       
            ColumnDefn("Harga Satuan", "right", 150, "harga", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateHarga),
            ColumnDefn("Qty", "right", 150, "qty", 
                       stringConverter=integerFormatString, 
                       cellEditorCreator=makeNumCtrlEditor,
                       valueSetter=self.updateQty),
            ColumnDefn("Sub Total", "right", 200, "subTotal", 
                       stringConverter=integerFormatString, isEditable=False)
        ])
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        self.objListDetail.Bind(wx.EVT_LIST_INSERT_ITEM, lambda(event): self.hitungTotal())
        self.objListDetail.Bind(wx.EVT_LIST_DELETE_ITEM, lambda(event): self.hitungTotal())
        
        self.buttonTambah.Bind(wx.EVT_BUTTON, self.OnTambahClick)
        self.buttonSimpan.Bind(wx.EVT_BUTTON, self.OnSimpanClick)
        
    def updateHarga(self, model, value):
        model.harga = value
    
    def updateQty(self, model, value):
        model.qty = value
        
    def HitungSubTotal(self, event):
        qty = self.numQty.GetValue()
        harga = self.numHarga.GetValue()
        self.numSubTotal.SetValue(qty * harga)
        
    def HargaPenjualan(self, barang, customer):
    	hargaJual = pj.HargaJual.query.filter_by(barang=barang, customer=customer).first()
    	if hargaJual is None:
    		harga = 0;
    	else:
    		harga = hargaJual.harga
        return harga
       
    def setHargaPenjualan(self):
    	n = self.comboBarang.GetSelection()
        if n == wx.NOT_FOUND:
            return
        else:
            barang = self.comboBarang.GetClientData(n)

        n = self.comboCustomer.GetSelection()
        if n == wx.NOT_FOUND:
        	return
        else:
        	customer = self.comboCustomer.GetClientData(n)
        self.numHarga.SetValue(self.HargaPenjualan(barang, customer))

    def OnBarangComboBox(self, event):
        self.setHargaPenjualan()

    def OnTambahClick(self, event):        
        n = self.comboBarang.GetSelection()
        if n == wx.NOT_FOUND:
            msg = wx.MessageDialog(self, "Barang belum dipilih", "Transaksi Retur Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboBarang.SetFocus()
            return
        else:
            barang = self.comboBarang.GetClientData(n)
            
        harga = self.numHarga.GetValue()
        qty = self.numQty.GetValue()
        
        if harga <= 0:
            msg = wx.MessageDialog(self, "Harga salah", "Transaksi Retur Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numHarga.SetFocus()
            return
            
        try:
            stock = pj.Barang.query.filter_by(id=barang.id).one()
        except:
            stock = 0
        
        if qty <= 0:
            msg = wx.MessageDialog(self, "Qty salah", "Transaksi Retur Pembelian", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.numQty.SetFocus()
            return
        
        #self.objListDetail.AddObject()
        
        detail = ItemDetail()
        detail.barang = barang
        detail.harga = harga
        detail.qty = qty
        exist = False
        for item  in self.objListDetail.GetObjects():
            if detail.barang == item.barang and detail.harga == item.harga:
                item.qty = item.qty + detail.qty
                exist = True
                self.objListDetail.RefreshObject(item)
                self.hitungTotal()                
                break
        if not exist:
            self.objListDetail.AddObject(detail)
        
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        self.comboBarang.SetFocus()

    def clearAllInput(self):
    	self.comboCustomer.SetSelection(wx.NOT_FOUND)
        self.comboCustomer.SetValue("")
        self.pickerTanggal.SetValue(wx.DateTime.Today())
        self.textNoRetur.Clear()
        self.comboBarang.SetSelection(wx.NOT_FOUND)
        self.comboBarang.SetValue("")
        self.numHarga.Clear()
        self.numQty.Clear()
        self.numSubTotal.Clear()
        self.objListDetail.ClearAll()
        self.numTotal.Clear()
        
    def hitungTotal(self):
        total = 0
        for item in self.objListDetail.GetObjects():
            total = total + item.subTotal()
            
        self.numTotal.SetValue(total)

    def OnSimpanClick(self, event):
        n = self.comboCustomer.GetSelection()
        if n == wx.NOT_FOUND:
            msg = wx.MessageDialog(self, "Customer belum diisi", "Transaksi Retur Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboCustomer.SetFocus()
            return
        else:
            customer = self.comboCustomer.GetClientData(n)
        
        noRetur = self.textNoRetur.GetValue().strip().upper()
        if noRetur == "":
            msg = wx.MessageDialog(self, "No Retur belum diisi", "Transaksi Retur Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.textNoRetur.SetFocus()
            return
        
        if self.objListDetail.GetItemCount() <= 0:
            msg = wx.MessageDialog(self, "Barang retur belum diisi", "Transaksi Retur Penjualan", wx.OK|wx.ICON_INFORMATION)
            msg.ShowModal()
            self.comboBarang.SetFocus()
            return
        
        tanggal = self.pickerTanggal.GetValue()
        tanggal = date(tanggal.Year, tanggal.Month+1, tanggal.Day)
        
        returPenjualan = pj.ReturPenjualanHeader()
        returPenjualan.noFaktur = noRetur
        returPenjualan.customer = customer
        returPenjualan.tanggal = tanggal
        
        details = []
        
        for item in self.objListDetail.GetObjects():
            detail = pj.ReturPenjualanDetail()
            detail.barang = item.barang
            detail.harga = item.harga
            detail.qty = item.qty
            
            stock = pj.Stock()
            stock.barang = item.barang
            stock.tanggal = tanggal
            stock.hargaModal = item.harga
            stock.qty = item.qty
            details.append(detail)
            
        returPenjualan.details = details
        pj.session.commit()
        
        self.clearAllInput()
        self.comboCustomer.SetFocus()    