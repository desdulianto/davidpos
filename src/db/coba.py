'''
Created on 20 Mar 2010

@author: David
'''
from elixir import *
from datetime import date

metadata.bind = "sqlite:///:memory:"
#metadata.bind.echo = True
session.autoflush = False

class BeliHeader(Entity):
    noFaktur = Field(String)
    tanggal = Field(Date)
    
    detail = OneToMany('BeliDetail')
    bayar = OneToMany('BayarDetail')
    
    def total(self):
        t = 0
        for item in self.detail:
            t = t + item.subTotal()
        return t
    
    def totalBayar(self):
        t = 0
        for item in self.bayar:
            t = t + item.jumlah
        return t
    
    def sisaBayar(self):
        return self.total() - self.totalBayar()
    
    def lunas(self):
        return self.sisaBayar() == 0

class BeliDetail(Entity):
    header = ManyToOne('BeliHeader')
    barang = Field(String)
    harga = Field(Integer)
    qty = Field(Integer)
    
    def subTotal(self):
        return self.harga * self.qty
    
class BayarHeader(Entity):
    noFaktur = Field(String)
    tanggal = Field(Date)
    
    detail = OneToMany('BayarDetail')
    
class BayarDetail(Entity):
    header = ManyToOne('BayarHeader')
    faktur = ManyToOne('BeliHeader')
    jumlah = Field(Integer)    

setup_all(True)
create_all()

def bagus(header):
    s = BeliDetail.query.filter("header_id=:noFaktur").params(noFaktur=header.noFaktur).all()
    print len(s)
    return s


a = BeliHeader()
a.noFaktur = 'abc'
a.tanggal = date(2010, 3, 20)
a.detail = []

b = BeliDetail()
b.barang = 'b1'
b.harga = 1000
b.qty = 2
a.detail.append(b)
b = BeliDetail()
b.barang = 'b2'
b.harga = 1500
b.qty = 4
a.detail.append(b)

session.commit()



a = BeliHeader()
a.noFaktur = 'def'
a.tanggal = date(2010, 3, 20)
a.detail = []

b = BeliDetail()
b.barang = 'b1'
b.harga = 1000
b.qty = 20
a.detail.append(b)

b = BeliDetail()
b.barang = 'b2'
b.harga = 1500
b.qty = 100
a.detail.append(b)

session.commit()

a = BeliHeader.query.all()

for f in a:
    print f.noFaktur
    print f.tanggal
    for item in f.detail:
        print item.barang, item.harga, item.qty, item.subTotal()
    print f.total()
    print
    
p = BayarHeader()
p.noFaktur = '0001'
p.tanggal = date(2010, 3, 22)
p.detail = []

y = BayarDetail()
y.faktur = BeliHeader.query.all()[0]
y.jumlah = 8000
p.detail.append(y)

y = BayarDetail()
y.faktur = BeliHeader.query.all()[1]
y.jumlah = 100000
p.detail.append(y)

session.commit()


print p.noFaktur
print p.tanggal
for item in p.detail:
    print item.faktur.noFaktur, item.faktur.total(), item.jumlah, item.faktur.sisaBayar(), item.faktur.lunas()
print

bagus(a[0])