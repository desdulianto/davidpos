from elixir import *
from sqlalchemy import and_
#import SpiffGuard as sg

#engine = create_engine("sqlite:///pelitajaya.sqlite")
#session = scoped_session(sessionmaker(autoflush=False))



#__metadata__ = metadata
#__session__ = session

metadata.bind = "sqlite:///db.sqlite"
#metadata.bind.echo = True
session.autoflush = False

#guard = sg.DB(metadata.bind)
#guard.install()

class Person(Entity):
    using_options(inheritance="multi")
    id = Field(Integer, primary_key=True, autoincrement=True)
    lebihBayar = OneToMany("LebihBayar")
    giro = OneToMany("Giro")
    
    def totalDeposit(self):
        total = 0
        for i in self.lebihBayar:
            if i.sisa() <= 0: continue
            total += i.jumlah
        return total

class Supplier(Person):
    using_options(inheritance="multi")    
    kode = Field(String, primary_key=True, unique=True)
    nama = Field(String)
    alamat = Field(String)
    kota = Field(String)
    noTelp = Field(String)
#    limitKredit = Field(Integer)
#    tempoKredit = Field(Integer)
    
#    lebihBayar = Field(Integer)
    
#    pembelian = OneToMany('BeliHeader', order_by=["tanggal","jatuhTempo"])
    pembelian = OneToMany('BeliHeader')
    pembayaran = OneToMany('BayarBeliHeader')
    
    def fakturBelumLunas(self, periode=None):
#        faktur = []
#        if periode is None:
#            for f in self.pembelian:
#                if not f.is_lunas():
#                    faktur.append(f)
        faktur = [f for f in self.pembelian if not f.is_lunas()]
        return faktur
    
    def __repr__(self):
        #return "<Supplier('%s', '%s')>" % (self.kode, self.nama)
        return self.display()
    
    def display(self):
        return "%s | %s" % (self.kode, self.nama)
        
class Customer(Person): #IGNORE:W0232
    using_options(inheritance="multi")
    kode = Field(String, primary_key=True, unique=True)
    nama = Field(String)
    alamat = Field(String)
    kota = Field(String)
    noTelp = Field(String)
#    limitKredit = Field(Integer)
    
#    lebihBayar = Field(Integer)

    hargaJual = OneToMany('HargaJual')
    penjualan = OneToMany('JualHeader')
    pembayaran = OneToMany('BayarJualHeader')
    
    
    
    def fakturBelumLunas(self, periode=None):
        faktur = []
        if periode is None:
            for f in self.penjualan:
                if not f.is_lunas():
                    faktur.append(f)
        return faktur
    
#    def __repr__(self):
#        return "<Customer('%s', '%s')>" % (self.kode, self.nama)
    def __repr__(self):
        return self.display()
    
    def display(self):
        return "%s | %s" % (self.kode, self.nama)

#class Sales(Person):
#    using_options(inheritance="multi")
#    kode = Field(String, primary_key=True, unique=True)
#    nama = Field(String)
#    
#    penjualan = OneToMany('JualHeader')
#    
#    def __repr__(self):
#        return "<Sales('%s', '%s')>" % (self.kode, self.nama)
#    
#    def display(self):
#        return "%s | %s" % (self.kode, self.nama)
    
class Kategori(Entity):
    id = Field(Integer, primary_key=True, autoincrement=True)
    nama = Field(String)

    def __repr__(self):
        return "<Kategori('%s')>" % self.nama
    
    def __unicode__(self):
        return '%s' % self.nama

    
class Barang(Entity):
    
#    id = Field(String, primary_key=True)
    kode = Field(String, primary_key=True)
    nama = Field(String)
    hargaModal = Field(Integer, default=0)
    hargaJual = Field(Integer, default=0)
    #satuan = Field(String)
    satuan = ManyToOne('Satuan') 
    kategori = ManyToOne('Kategori')    
    
    daftarHarga = OneToMany('HargaJual')
    stock = OneToOne('Stock')
#    pembelian = OneToMany('BeliDetail')
#    penjualan = OneToMany('JualDetail')
#    
#    returPembelian = OneToMany('ReturPembelianDetail')
#    returPenjualan = OneToMany('ReturPenjualanDetail')
    
    transaksi = OneToMany("Transaksi")
    transaksiStock = OneToMany("TransaksiStock")

    def hargaRata2(self):
        if self.stock is None:
            return self.hargaModal
        return self.stock.hargaModal
    
    def namaSatuan(self):
        return self.satuan.nama
    
    def namaKategori(self):
        return self.kategori.nama if self.kategori is not None else ''
    
    def jumlahStock(self, tanggal=None, awal=True):
#        jumlah = 0
#        # saldo stock
#        if self.stock:
#            jumlah = jumlah + self.stock.qty
#        # tambah/kurang transaksi
#        trans = Transaksi.query.filter(and_(Transaksi.barang == self,
#                                            Transaksi.posted == False))
#                                       
#        for tran in trans:
#            if isinstance(tran, BeliDetail):
#                jumlah = jumlah + tran.qty
#            elif isinstance(tran, ReturPembelianDetail):
#                jumlah = jumlah - tran.qty
#            elif isinstance(tran, JualDetail):
#                jumlah = jumlah - tran.qty
#            elif isinstance(tran, ReturPenjualanDetail):
#                jumlah = jumlah + tran.qty            
#            else:
#                jumlah = jumlah + tran.qty
#        return jumlah
        if self.stock:
            jumlah = self.stock.qty
        else:
            jumlah = 0
        return jumlah
    
    def nilaiPersediaan(self):
        return self.stock.qty * self.hargaModal
    
    def __repr__(self):
        return "<Barang('%s', '%s')>" % (self.kode, self.nama)
    
    def __unicode__(self):
        return u'%s %s' % (self.kode, self.nama)
    
    def display(self):
        return "%s | %s" % (self.kode, self.nama)

class HargaJual(Entity):
    barang = ManyToOne('Barang', primary_key=True)
    customer = ManyToOne('Customer', primary_key=True)
    harga = Field(Integer)
    
    def namaBarang(self):
        return self.barang.display()
    
    def __repr__(self):
        return "<HargaJual('%s', '%s', %d)>" % (self.barang, self.customer, self.harga)

class Stock(Entity):
    barang = ManyToOne('Barang')
    tanggal = Field(Date)
    hargaModal = Field(Integer)
    qty = Field(Integer)
    
    def __repr__(self):
        return "<Stock('%s', %d, %d)>" % (self.barang, self.hargaModal, self.qty)
    
class Faktur(Entity):   #IGNORE:W0232
    using_options(inheritance="multi")
    
    details = OneToMany("Transaksi")
    pembayaran = OneToMany("TransaksiBayar")
    tanggal = Field(Date)
    lunas = Field(Boolean)
    createdBy = ManyToOne('User')
    
    def total(self):
        t = 0
        for item in self.details:
            t = t + item.subTotal()
        
        return t
    
    def totalNet(self):
        t = self.total()
        t = t - self.retur - self.disc
        return t

    def totalBayar(self):
#        t = 0
#        for item in self.pembayaran:
##            if item.header.giro is not None and not item.header.giro.clear:
##                continue
#            if item.header.giro is not None:
#                lunas = True
#                for g in item.header.giro:
#                    if not g.clear:
#                        lunas = False
#                        break
#                if lunas:
#                    continue
#            print item, item.jumlah
#            t = t + item.jumlah
#        return t
        t = 0
            
        for item in self.pembayaran:
#            if item.header.giro is not None:
#                lunas = True
#                for g in item.header.giro:
#                    if not g.clear:
#                        lunas = False
#                        break
#                if not lunas:
#                    continue            
            t = t + item.jumlah
        return t
    
    def totalGiroBelumClear(self):
        t = 0
        for item in self.pembayaran:
            if item.header.giro is None:
                continue
#            elif item.header.giro is not None and item.header.giro.clear:
#                continue
            else:
                for g in item.header.giro:
                    if g.clear:
                        continue
                    else:
                        t = t + item.jumlah
        return t

    def sisaBayar(self):
        return self.total() - self.totalBayar()

    def is_lunas(self):
#        t = False
#        if self.lunas is None:
#            t = self.sisaBayar() <= 0
#            if t:
#                self.lunas = t
#        else:
#            t = self.lunas
#        return t
        return self.sisaBayar() <= 0
    
    def lebihBayar(self):
        return self.sisaBayar() < 0


class Transaksi(Entity):
    using_options(inheritance="multi") 
    barang = ManyToOne("Barang")
#    qty = Field(Integer)
    
    header = ManyToOne("Faktur")
    transaksiStock = ManyToOne("TransaksiStock")
    posted = Field(Boolean, default=False)    

class BeliHeader(Faktur):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
#    tanggal = Field(Date)
    jatuhTempo = Field(Date)
    supplier = ManyToOne('Supplier')
#    lunas = Field(Boolean)
    
#    details = OneToMany('BeliDetail')
#    pembayaran = OneToMany('BayarBeliDetail')
    
#    def total(self):
#        t = 0
#        for item in self.details:
#            t = t + item.subTotal()
#        return t

#    def totalBayar(self):
#        t = 0
#        for item in self.pembayaran:
#            t = t + item.jumlah
#        return t
    
#    def sisaBayar(self):
#        return self.total() - self.totalBayar()
    
#    def is_lunas(self):
#        return self.sisaBayar() <= 0
#    
#    def lebihBayar(self):
#        return self.sisaBayar() < 0
        
    
    def __repr__(self):
        return "<BeliHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier)
    
class BeliDetail(Transaksi):
    using_options(inheritance="multi")
#    barang = ManyToOne("Barang")
    qty = Field(Integer)
    harga = Field(Integer)    
    
    
    def namaBarang(self):
        return self.barang.display()
    
    def subTotal(self):
        return self.harga * self.qty
    
    def __repr__(self):
        return "<BeliDetail('%s', '%s', %d, %d)>" % (self.header, self.barang, self.harga, self.qty)
    
class JualHeader(Faktur):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
#    tanggal = Field(Date)
    customer = ManyToOne('Customer')
#    sales = ManyToOne('Sales')
#    lunas = Field(Boolean)
    
#    details = OneToMany('JualDetail')
#    pembayaran = OneToMany('BayarJualDetail')

#    def total(self):
#        t = 0
#        for item in self.details:
#            t = t + item.subTotal()
#        return t
#
#    def totalBayar(self):
#        t = 0
#        for item in self.pembayaran:
#            t = t + item.jumlah
#        return t
#    
#    def sisaBayar(self):
#        return self.total() - self.totalBayar()
#    
#    def lunas(self):
#        return self.sisaBayar() <= 0
#    
#    def lebihBayar(self):
#        return self.sisaBayar() < 0
    
    def __repr__(self):
        return "<JualHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
class JualDetail(Transaksi):
    using_options(inheritance="multi")
#    header = ManyToOne('JualHeader', primary_key=True)
#    barang = ManyToOne('Barang', primary_key=True)
    hargaModal = Field(Integer, primary_key=True)
    hargaJual = Field(Integer, primary_key=True)
    disc = Field(Integer)
    qty = Field(Integer)
    #details = OneToMany('BarangJualDetail')
    
    def namaBarang(self):
        return self.barang.display()
    
    def jumlah(self):
        jumlah = 0
        for item in self.details:
            jumlah = jumlah + item.qty
        return jumlah
    
    def subTotal(self):
        return self.qty * (self.hargaJual - self.disc)          
    
    def __repr__(self):
        return "<JualDetail('%s', '%s', %d, %d)>" % (self.header, self.barang.nama, 
                                                     self.hargaJual, self.qty)
        
class ReturPembelianHeader(Faktur):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
#    tanggal = Field(Date)
    supplier = ManyToOne('Supplier')
 #   details = OneToMany('ReturPembelianDetail')
    
    def __repr__(self):
        return "<ReturPembelianHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)

class ReturPembelianDetail(Transaksi):
    using_options(inheritance="multi")
#    header = ManyToOne('ReturPembelianHeader')
    #barang = ManyToOne('Stock')
#    barang = ManyToOne('Barang')
    harga = Field(Integer)
    qty = Field(Integer)
    
    def subTotal(self):
        return self.qty * self.harga              
    
    def __repr__(self):
        return "<ReturPembelianDetail('%s', '%s', %d, %d>" % (self.header, self.barang.nama, self.harga, self.qty)
    
class ReturPenjualanHeader(Faktur):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
#    tanggal = Field(Date)
    customer = ManyToOne('Customer')
    
#    details = OneToMany('ReturPenjualanDetail')
    
    def __repr__(self):
        return "<ReturPenjualanHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
class ReturPenjualanDetail(Transaksi):
    using_options(inheritance="multi")
#    header = ManyToOne('ReturPenjualanHeader')
    #barang = ManyToOne('Stock')
#    barang = ManyToOne('Barang')
    harga = Field(Integer)
    qty = Field(Integer)

    def subTotal(self):
        return self.qty * self.harga    

    def __repr__(self):
        return "<ReturPenjualanDetail('%s', '%s', %d, %d>" % (self.header, self.barang, self.harga, self.qty)
        
#class BeliHeader(Entity):
#    noFaktur = Field(String, primary_key=True)
#    tanggal = Field(Date)
#    jatuhTempo = Field(Date)
#    supplier = ManyToOne('Supplier')
#    lunas = Field(Boolean)
#    
#    details = OneToMany('BeliDetail')
#    pembayaran = OneToMany('BayarBeliDetail')
#    
#    def total(self):
#        t = 0
#        for item in self.details:
#            t = t + item.subTotal()
#        return t
#
#    def totalBayar(self):
#        t = 0
#        for item in self.pembayaran:
#            t = t + item.jumlah
#        return t
#    
#    def sisaBayar(self):
#        return self.total() - self.totalBayar()
#    
#    def lunas(self):
#        return self.sisaBayar() <= 0
#    
#    def lebihBayar(self):
#        return self.sisaBayar() < 0
#        
#    
#    def __repr__(self):
#        return "<BeliHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier)
#    
#    def display(self):
#        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier)
#
#class BeliDetail(Entity):
#    header = ManyToOne('BeliHeader')
#    barang = ManyToOne('Barang')
#    harga = Field(Integer)
#    qty = Field(Integer)
#    
#    def namaBarang(self):
#        return self.barang.display()
#    
#    def subTotal(self):
#        return self.harga * self.qty
#    
#    def __repr__(self):
#        return "<BeliDetail('%s', '%s', %d, %d)>" % (self.header, self.barang, self.harga, self.qty)
        
#class JualHeader(Entity):
#    noFaktur = Field(String, primary_key=True)
#    tanggal = Field(Date)
#    customer = ManyToOne('Customer')
#    sales = ManyToOne('Sales')
#    lunas = Field(Boolean)
#    
#    details = OneToMany('JualDetail')
#    pembayaran = OneToMany('BayarJualDetail')
#
#    def total(self):
#        t = 0
#        for item in self.details:
#            t = t + item.subTotal()
#        return t
#
#    def totalBayar(self):
#        t = 0
#        for item in self.pembayaran:
#            t = t + item.jumlah
#        return t
#    
#    def sisaBayar(self):
#        return self.total() - self.totalBayar()
#    
#    def lunas(self):
#        return self.sisaBayar() <= 0
#    
#    def lebihBayar(self):
#        return self.sisaBayar() < 0
#    
#    def __repr__(self):
#        return "<JualHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
#    
#    def display(self):
#        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.customer.nama)
#    
#class JualDetail(Entity):
#    header = ManyToOne('JualHeader', primary_key=True)
#    barang = ManyToOne('Barang', primary_key=True)
#    hargaModal = Field(Integer, primary_key=True)
#    hargaJual = Field(Integer, primary_key=True)
#    disc = Field(Integer)
#    qty = Field(Integer)
#    #details = OneToMany('BarangJualDetail')
#    
#    def namaBarang(self):
#        return self.barang.display()
#    
#    def jumlah(self):
#        jumlah = 0
#        for item in self.details:
#            jumlah = jumlah + item.qty
#        return jumlah
#    
#    def subTotal(self):
#        return self.qty * self.hargaJual            
#    
#    def __repr__(self):
#        return "<JualDetail('%s', '%s', %d, %d)>" % (self.header, self.barang.nama, 
#                                                     self.hargaJual, self.qty)

#class BarangJualDetail(Entity):
#    detail = ManyToOne('JualDetail')
#    hargaBeli = Field(Integer)
#    qty = Field(Integer)
#    
#    def __repr__(self):
#        return "<BarangJualDetail('%s', %d, %d)>" % (self.detail, self.hargaBeli, self.qty)
    
    
#class ReturPembelianHeader(Entity):
#    noFaktur = Field(String, primary_key=True)
#    tanggal = Field(Date)
#    supplier = ManyToOne('Supplier')
#    details = OneToMany('ReturPembelianDetail')
#    
#    def __repr__(self):
#        return "<ReturPembelianHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
#    
#    def display(self):
#        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
#
#class ReturPembelianDetail(Entity):
#    header = ManyToOne('ReturPembelianHeader')
#    #barang = ManyToOne('Stock')
#    barang = ManyToOne('Barang')
#    harga = Field(Integer)
#    qty = Field(Integer)
#    
#    def __repr__(self):
#        return "<ReturPembelianDetail('%s', '%s', %d, %d>" % (self.header, self.barang.nama, self.harga, self.qty)
    
#class ReturPenjualanHeader(Entity):
#    noFaktur = Field(String, primary_key=True)
#    tanggal = Field(Date)
#    customer = ManyToOne('Customer')
#    
#    details = OneToMany('ReturPenjualanDetail')
#    
#    def __repr__(self):
#        return "<ReturPenjualanHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
#    
#    def display(self):
#        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
#    
#class ReturPenjualanDetail(Entity):
#    header = ManyToOne('ReturPenjualanHeader')
#    #barang = ManyToOne('Stock')
#    barang = ManyToOne('Barang')
#    harga = Field(Integer)
#    qty = Field(Integer)
#
#    def __repr__(self):
#        return "<ReturPenjualanDetail('%s', '%s', %d, %d>" % (self.header, self.barang, self.harga, self.qty)

# untuk mencatat transaksi keluar & masuks stock
class TransaksiStock(Entity):
    using_options(inheritance="multi")
    id = Field(Integer, primary_key=True, autoincrement=True)
    tanggal = Field(Date)
    barang = ManyToOne("Barang")
    jenis = Field(Enum("DEBET", "KREDIT"))    
    qty = Field(Integer)
    hargaModal = Field(Integer)
    hargaJual = Field(Integer)
    keterangan = Field(String)
    detailFaktur = OneToOne("Transaksi")

# faktur pembayaran
class FakturBayar(Entity):
    using_options(inheritance="multi")
    details = OneToMany("TransaksiBayar")
#    giro = OneToOne("Giro", inverse="fakturBayar")
    giro = ManyToMany("Giro")
    lebihBayar = ManyToOne("LebihBayar")
    dariLebihBayar = ManyToOne("PembayaranLebihBayar")  
    
    def total(self):
        t = 0
        for detail in self.details:
            t = t + detail.jumlah
        return t

# details untuk faktur pembayaran    
class TransaksiBayar(Entity):
    using_options(inheritance="multi")
    faktur = ManyToOne("Faktur") 
    header = ManyToOne("FakturBayar")
    jumlah = Field(Integer)
        
class BayarBeliHeader(FakturBayar):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
    supplier = ManyToOne('Supplier')
    tanggal = Field(Date)
#    giro = OneToOne("GiroKeluar", inverse="pembayaran")
    
#    details = OneToMany('BayarBeliDetail')
    
    def __repr__(self):
        return "<BayarBeliHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.supplier.nama)
    
#class BayarBeliDetail(Entity):
#    header = ManyToOne('BayarBeliHeader')
#    fakturPembelian = ManyToOne('BeliHeader')
#    jumlah = Field(Integer)

class BayarBeliDetail(TransaksiBayar):
    using_options(inheritance="multi")
    
class BayarJualHeader(FakturBayar):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
    customer = ManyToOne('Customer')
    tanggal = Field(Date)
    
#    details = OneToMany('BayarJualDetail')

    def __repr__(self):
        return "<BayarJualHeader('%s', '%s', '%s')>" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
    def display(self):
        return "%s | %s | %s" % (self.noFaktur, str(self.tanggal), self.customer.nama)
    
#class BayarJualDetail(Entity):
#    header = ManyToOne('BayarJualHeader')
#    fakturPenjualan = ManyToOne('JualHeader')
#    jumlah = Field(Integer)

class BayarJualDetail(TransaksiBayar):
    using_options(inheritance="multi")
    
class LebihBayar(Entity):
    using_options(inheritance="multi")
    person = ManyToOne("Person")
    fakturBayar = OneToOne("FakturBayar")
    jumlah = Field(Integer)
    terpakai = Field(Integer)
    
    pembayaran = OneToMany("PembayaranLebihBayar")
    
    def sisa(self):
        return self.jumlah - self.terpakai
    
class PembayaranLebihBayar(Entity):
    using_options(inheritance="multi")
    fakturBayar = OneToOne("FakturBayar")
    jumlah = Field(Integer)
    lebihBayar = ManyToOne("LebihBayar")
  
class Satuan(Entity):
    nama = Field(String, primary_key=True)
    barang = OneToMany('Barang')
    
    def __repr__(self):
        return "<Satuan('%s')>" % self.nama
    
    def __unicode__(self):
        return u'%s' % self.nama

    def display(self):
        return self.nama    

class Bank(Entity):
    noRekening = Field(String, primary_key=True)
    nama = Field(String, primary_key=True)
    
    transaksi = OneToMany('TransaksiBank')
    
    def saldo(self):
        totalDebet = 0
        totalKredit = 0
        for t in self.transaksi:
            if t.jenis == "KREDIT":
                totalKredit = totalKredit + t.jumlah
            else:
                totalDebet = totalDebet + t.jumlah
        return totalDebet - totalKredit
    
    def __repr__(self):
        return "<Bank('%s', '%s')>" % (self.noRekening, self.nama)
    
    def display(self):
        return "%s | %s" % (self.nama, self.noRekening)
    
class TransaksiBank(Entity):
#    id = Field(Integer, primary_key=True, autoincrement=True)
    bank = ManyToOne('Bank')
    tanggal = Field(Date)
    keterangan = Field(String)
    jenis = Field(Enum("DEBET", "KREDIT"))
    jumlah = Field(Integer)
    
#    giro = OneToOne('Giro')
    
#class Giro(Entity):
#    noGiro = Field(String, primary_key=True)
#    bank = ManyToOne('Bank')
#    jatuhTempo = Field(Integer)
#    bayarJual = OneToOne('BayarJualHeader')
#    bayarBeli = OneToOne('BayarBeliHeader')
#    jumlah = Field(Integer)
#    transaksiBank = OneToOne('TransaksiBank')

# giro yang keluar untuk pembayaran
class Giro(Entity):
    tanggal = Field(Date)
    noGiro = Field(String, primary_key=True)
    bank = ManyToOne("Bank", primary_key=True)
    person = ManyToOne("Person")
#    jenis = Field(String, primary_key=True) # jenis giro, masuk 
#                                            # (pembayaran dari 
#                                            # pelanggan)/keluar 
#                                            # (pembayaran ke supplier)
    jenis = Field(Enum("KELUAR", "MASUK", "TRANSFER"))
    jatuhTempo = Field(Date)
    jumlah = Field(Integer)
    clear = Field(Boolean)
#    fakturBayar = ManyToOne("FakturBayar")
    fakturBayar = ManyToMany("FakturBayar")
    
    def total(self):
        #return self.fakturBayar[0].total()
        return self.jumlah
    
    def __repr__(self):
        return "<Giro('%s', '%s')>" % (self.bank, self.noGiro)
    
class PenyesuaianStockHeader(Faktur):
    using_options(inheritance="multi")
    noFaktur = Field(String, primary_key=True, unique=True)
#    tanggal = Field(Date)
    keterangan = Field(String)

    def __repr__(self):
        return "<PenyesuaianStockHeader('%s', '%s')>" % (self.noFaktur, str(self.tanggal))
    
    def display(self):
        return "%s | %s" % (self.noFaktur, str(self.tanggal))

    

class PenyesuaianStockDetail(Transaksi):
    using_options(inheritance="multi")
    harga = Field(Integer)
    disc = Field(Integer)
    qty = Field(Integer)
    
    def namaBarang(self):
        return self.barang.display()
    
    def jumlah(self):
        jumlah = 0
        for item in self.details:
            jumlah = jumlah + item.qty
        return jumlah
    
    def subTotal(self):
        return self.qty * (self.hargaJual - self.disc)          
    
    def __repr__(self):
        return "<PenyesuaianStockDeetail('%s', '%s', %d, %d)>" % (self.header, self.barang.nama, 
                                                     self.harga, self.qty)
    
    
class User(Entity):
    user = Field(String, primary_key=True)
    password = Field(String)
    active = Field(Boolean)
    acls = OneToMany("ACL")
    
    def has_permission(self, act, res):
        for acl in self.acls:
            if acl.resource.module == res and acl.action.nama == act:
                return True
        return False
    
    def update_permission(self, res, act):
        # disable permission
        for acl in self.acls:
            if acl.resource.module == res and acl.action.nama == act:
                acl.delete()
                session.commit()
                return True
        # add permission       
        try:
            resource = Resource.query.filter(Resource.module == res).one()
            action = Action.query.filter(Action.nama == act).one()
            acl = ACL()
            acl.user = self
            acl.resource = resource
            acl.action = action
            session.commit()
            return True
        except Exception, e:
            return False
    
    def __repr__(self):
        return "<User('%s')>" % self.user
    
    def display(self):
        return "%s" % self.user
    
class Resource(Entity):
    module = Field(String, primary_key=True)
    nama = Field(String)
    acls = OneToMany("ACL")
    
    def __repr__(self):
        return "<Resource('%s')>" % self.module
    
    def display(self):
        return "%s" % self.module
    
class Action(Entity):
    nama = Field(String, primary_key=True)
    acls = OneToMany("ACL")
    
    def __repr__(self):
        return "<Action('%s')>" % self.nama
    
    def display(self):
        return "%s" % self.nama
           
class ACL(Entity):
    user = ManyToOne('User')
    resource = ManyToOne('Resource')
#    permissions = Field(String)
    action = ManyToOne("Action")
    
    def __repr__(self):
        return "<ACL('%s', '%s', '%s')>" % (self.user, self.resource, self.action)
    
    def display(self):
        return "%s | %s | %s" % (self.user, self.resource, self.action)

#class User(sg.Resource):
#    password = Field(String)
#    
#guard.register_type([User])
    
setup_all(True)
create_all()

if not User.query.filter_by(user='ADMIN').all():
    from hashlib import sha1
    u = User()
    u.user = 'ADMIN'
    u.password = sha1('admin').hexdigest()
    u.active = True
    
    actions = ("TAMBAH", "LIHAT", "HAPUS", "UBAH", "CETAK", "ADMIN")
    for action in actions:
        a = Action()
        a.nama = action
        
    # resources
    resources = (("DATABARANG", "Data Barang"), ("DATACUSTOMER", "Data Customer"), 
                 ("DATASUPPLIER", "Data Supplier"), ("DATASALES", "Data Sales"),
                 ("DATAHARGAJUAL", "Data Harga Jual"), ("DATASATUAN", "Data Satuan"),
                 ("DATAKATEGORI", "Data Kategori"), 
                 ("DATABANK", "Data Bank"), ("DATAPENYESUAIANSTOCK", "Data Penyesuaian Stock"),
                 ("SUPPLIERPEMBELIAN", "Pembelian"), ("SUPPLIERRETURPEMBELIAN", "Retur Pembelian"), 
                 ("SUPPLIERPEMBAYARAN", "Pembayaran Supplier"),
                 ("CUSTOMERPENJUALAN", "Penjualan"), ("CUSTOMERRETURPENJUALAN", "Retur Penjualan"), 
                 ("CUSTOMERPEMBAYARAN", "Pembayaran Customer"),
                 ("BANKTRANSAKSI", "Transaksi Bank"), ("BANKREMINDERGIRO", "Reminder Giro"), 
                 ("BANKPROSESPEMBAYARANGIRO", "Proses Pembayaran Giro"),
                 ("BANKGIRO", "Giro"), ("USERADMINISTRASIPENGGUNA", "Administrasi Pengguna"), 
                 ("USERUBAHPASSWORD", "Ubah Password"),
                 ("LAPORANTRANSAKSISTOCK", "Laporan Transaksi Stock"),
                 ("LAPORANSTOCK", "Laporan Stock"), ("LAPORANPENJUALAN", "Laporan Penjualan"), 
                 ("LAPORANPEMBELIAN", "Laporan Pembelian"),
                 ("LAPORANRETURPENJUALAN", "Laporan Retur Penjualan"), 
                 ("LAPORANRETURPEMBELIAN", "Laporan Retur Pembelian"),
                 ("LAPORANPEMBAYARANSUPPLIER", "Laporan Pembayaran Supplier"), 
                 ("LAPORANPEMBAYARANCUSTOMER", "Laporan Pembayaran Customer"),
                 ("PEMBAYARANGIROSUPPLIER", "Pembayaran Giro Supplier"),
                 ("PEMBAYARANGIROCUSTOMER", "Pembayaran Giro Customer"))

    for resource in resources:
        r = Resource()
        r.module = resource[0]
        r.nama = resource[1]

    session.commit()

    # acls
    user = User.query.filter(User.user == "ADMIN").one()
    for resource in Resource.query.all():
        for action in Action.query.all():
            acl = ACL()
            acl.user = user
            acl.resource = resource
            acl.action = action
    
    # customer
    customer = Customer()
    customer.kode = "CASH"
    customer.nama = "CASH"
    customer.alamat = "-"
    customer.noTelp = "-"
    customer.kota = "-"
    
    session.commit()
    

#raw_input()
