"""Subclass of FrameGiro, which is generated by wxFormBuilder."""

import wx
import gui

from ObjectListView import ColumnDefn, ObjectListView as olv
from datetime import date
from db import pelitajaya as pj
from util import populateComboBox, dateFormatString, integerFormatString, wxDateToPyDate
from ObjectListView import ObjectListView as olv
from DialogKalendar import DialogKalendar

import FramePembayaranHutang

from controller import auth

from sqlalchemy import or_

class Giro(object):
    def __init__(self, giro=None):
        self.giro = giro
        
    def bank(self):
        return self.giro.bank
    
    def person(self):
        return self.giro.person
    
    def jatuhTempo(self):
        return self.giro.jatuhTempo
    
    def jumlah(self):
        return self.giro.jumlah
    
    def clear(self):
        return self.giro.clear
    
    def jenis(self):
        return self.giro.jenis
    

        
# Implementing FrameGiro
class FrameGiro( gui.FrameGiro ):
    __nama__ = "BANKGIRO"
    
    def __init__( self, parent ):
        gui.FrameGiro.__init__( self, parent )
        
        # combo bank
        self.comboBank.SetFocus()
        bank = pj.Bank.query.filter(pj.Bank.nama != "KAS").order_by(pj.Bank.nama, pj.Bank.noRekening).all()
        self.comboBank.Append("<< Semua >>", None)
        populateComboBox(self.comboBank, bank)
        
        # bind event
        self.buttonCari.Bind(wx.EVT_BUTTON, self.OnCariClick)
        
        # objlistDetail
        self.objListDetail.SetColumns([
            ColumnDefn("Bank", "left", 200, "bank", isEditable=False,
                       stringConverter=self.formatBank),
            ColumnDefn("No. Giro", "left", 200, "giro", isEditable=False,
                       stringConverter=self.formatNoGiro),
            ColumnDefn("Nama", "left", 200, "person", isEditable=False),
            ColumnDefn("Jatuh Tempo", "left", 200, "jatuhTempo", isEditable=False,
                       stringConverter=dateFormatString),
            ColumnDefn("Jumlah", "right", 200, "jumlah", isEditable=False,
                       stringConverter=integerFormatString),
            ColumnDefn("Jenis", "left", 100, "jenis", isEditable=False),
            ColumnDefn("Clear", "right", 50, checkStateGetter="clear", isEditable=True, checkStateSetter=self.updateClear,
                       stringConverter=lambda x: "")            
                                       ])
        self.objListDetail.SetEmptyListMsg("Tidak ada data")
        #self.objListDetail.cellEditMode = olv.CELLEDIT_DOUBLECLICK
        
        if not auth.user.has_permission("UBAH", self.__nama__):
            self.objListDetail.Disable()        
        
    def formatBank(self, value):
        return value.nama
    
    def formatNoGiro(self, value):
        return value.noGiro
        
    def OnCariClick(self, event):
        # bank
        n = self.comboBank.GetSelection()
        bank = None
        if n != wx.NOT_FOUND:
            bank = self.comboBank.GetClientData(n)
        
        # no giro
        noGiro = self.textNomorGiro.GetValue().strip()
        
        # jenis
        jenis = self.radioBoxJenis.GetItemLabel(self.radioBoxJenis.GetSelection()).upper()
        
        # status
        status = self.radioBoxStatus.GetSelection()
        if status == 0:
            status = True
        elif status == 1:
            status = False
        else:
            status = None
            
        # tanggal
        tanggalMulai = wxDateToPyDate(self.pickerDariTanggal.GetValue())    
        tanggalSelesai = wxDateToPyDate(self.pickerSampaiTanggal.GetValue())        
        
        giros = pj.Giro.query
        
        if bank is not None:
            giros = giros.filter(pj.Giro.bank == bank)
            
        if noGiro != "":
            giros = giros.filter(pj.Giro.noGiro.like("%%%s%%" % noGiro))
            
        
        if jenis in ("KELUAR", "TRANSFER"):
            giros = giros.filter(or_(pj.Giro.jenis == jenis, pj.Giro.jenis == "TRANSFER"))
        elif jenis == "MASUK":
            giros = giros.filter(pj.Giro.jenis == jenis)
            
        if status in (True, False):
            giros = giros.filter(pj.Giro.clear == status)
                    
        if self.checkBoxTanggal.GetValue() == True:
            giros = giros.filter(pj.Giro.jatuhTempo.between(tanggalMulai, tanggalSelesai))
        else:
            giros = giros.filter(pj.Giro.jatuhTempo >= tanggalMulai)
        
            
        details = []
        for giro in giros:
            item = Giro(giro)
            details.append(item)
            
        self.objListDetail.SetObjects(details)
        
    def updateClear(self, obj, value):
        if not value:
            return
        
        kalendar = DialogKalendar(self)
        tanggal = None
        if kalendar.ShowModal() == wx.ID_OK:
            tanggal = kalendar.GetTanggal()
            tanggal = date(tanggal.Year, tanggal.Month+1, tanggal.Day)
        else:
            return
        obj.giro.clear = value
        
        if obj.giro.jenis in ("MASUK", "KELUAR"):        
            transaksi = pj.TransaksiBank()
            transaksi.bank = obj.giro.bank
            transaksi.tanggal = tanggal
            transaksi.keterangan = "CLEARING GIRO %s" % obj.giro.noGiro
            transaksi.jumlah = obj.giro.total()
            if obj.giro.jenis == "MASUK":
                transaksi.jenis = "DEBET"
            else:
                transaksi.jenis = "KREDIT"
                
            # bentuk faktur pembayaran
            if obj.giro.jenis in ("MASUK", "KELUAR"):
                if obj.giro.jenis == "KELUAR":
                    header = pj.BayarBeliHeader()
                    header.supplier = obj.giro.person
                else:
                    header = pj.BayarJualHeader()
                    header.customer = obj.giro.person
                header.noFaktur = "GIRO " + obj.giro.bank.nama + " " + obj.giro.noGiro                
                header.tanggal = tanggal                
                
                details = []
                jumlahGiro = obj.giro.total()
                for faktur in obj.giro.person.fakturBelumLunas():
                    sisa = faktur.sisaBayar()
                    
                    if obj.giro.jenis == "KELUAR":
                        detail = pj.BayarBeliDetail()
                        detail.supplier = obj.giro.person
                    else:
                        detail = pj.BayarJualDetail()
                        detail.customer = obj.giro.person
                        
                    detail.faktur = faktur
                    detail.header = header
                    
                    
                    if jumlahGiro <= sisa:
                        bayarDeposit = sisa - jumlahGiro
                        detail.jumlah = jumlahGiro
                        jumlahGiro = 0
                        
                        # pembayaran dari deposit
                        dariDeposit = []
                        
                        for item in obj.giro.person.lebihBayar:
                            if item.sisa() <= 0: continue
                            byr = pj.PembayaranLebihBayar()
                            byr.fakturBayar = header
                            byr.lebihBayar = item
                            if bayarDeposit <= item.sisa():
                                item.terpakai += bayarDeposit
                                byr.jumlah = bayarDeposit
                                detail.jumlah += bayarDeposit
                                bayarDeposit = 0                                
                            else:
                                itemsisa = item.sisa()
                                byr.jumlah = itemsisa
                                bayarDeposit -= itemsisa
                                item.terpakai += itemsisa
                                detail.jumlah += itemsisa
                            dariDeposit.append(byr)
                            if bayarDeposit <= 0:
                                break
                    else:
                        detail.jumlah = sisa
                        jumlahGiro -= sisa
                        
                    if jumlahGiro <= 0:
                        break
                    
                if jumlahGiro > 0:
                    # lebih bayar
                    lebihBayar = pj.LebihBayar()
                    lebihBayar.person = obj.giro.person
                    lebihBayar.fakturBayar = header
                    lebihBayar.jumlah = jumlahGiro
                    lebihBayar.terpakai = 0
                             
        pj.session.commit()
#        pj.session.rollback()