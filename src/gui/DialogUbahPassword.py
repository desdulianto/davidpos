"""Subclass of DialogUbahPassword, which is generated by wxFormBuilder."""

import wx
import gui

from controller import auth

# Implementing DialogUbahPassword
class DialogUbahPassword( gui.DialogUbahPassword ):
    __nama__ = "USERUBAHPASSWORD"
    
    def __init__( self, parent ):
        gui.DialogUbahPassword.__init__( self, parent )
        
        if auth.user is not None:
            self.staticNamaUser.SetLabel(auth.user.user)
            
        # bind events
        self.buttonUbahPassword.Bind(wx.EVT_BUTTON, self.OnUbahPassword)
        
        if not auth.user.has_permission("UBAH", self.__nama__):
            self.buttonUbahPassword.Disable()
        
    def OnUbahPassword(self, event):
        passwordLama = self.textPasswordLama.GetValue()
        passwordBaru = self.textPasswordBaru.GetValue()
        passwordBaru1 = self.textPasswordBaru1.GetValue()
        
        if passwordBaru == "" or passwordBaru1 == "":
            msg = wx.MessageDialog(self, "Password Baru belum diisi", "Ubah Password",
                                   wx.OK)
            msg.ShowModal()
            self.textPasswordBaru.SetFocus()
            return
        
        if passwordBaru != passwordBaru1:
            msg = wx.MessageDialog(self, "Password Baru tidak sama", "Ubah Password",
                                   wx.OK)
            msg.ShowModal()
            self.textPasswordBaru.SetFocus()
            return
    
        if not auth.checkPassword(auth.user, passwordLama):
            msg = wx.MessageDialog(self, "Password Lama salah", "Ubah Password",
                                   wx.OK)
            msg.ShowModal()
            self.textPasswordLama.SetFocus()
            return
        
        auth.updatePassword(auth.user, passwordBaru)
        msg = wx.MessageDialog(self, "Password sudah diubah", "Ubah Password",
                               wx.OK)
        msg.ShowModal()
        self.EndModal(wx.ID_OK)        